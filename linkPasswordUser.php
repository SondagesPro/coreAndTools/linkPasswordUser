<?php
/**
 * Send a one time password link
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019 Denis Chenu <http://www.sondages.pro>
 * @license GPL
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class linkPasswordUser extends PluginBase
{

    protected $storage = 'DbStorage';

    static protected $name = 'linkPasswordUser';
    static protected $description = 'Send a one time password link';

    static protected $dbVersion = 1;

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'linkTime' => array(
            'type'=>'int',
            'label'=>"Maximum delay in minute between link generation and connexion",
            'default'=>'60',
            'htmlOptions' => array(
                'min'=>1,
            ),
            'help' => "Set the time limit to delete hash.",
        ),
        'randomKeySize' => array(
            'type'=>'int',
            'label'=>"Size of key",
            'default'=>'42',
            'htmlOptions' => array(
                'min'=>5,
            ),
        ),
        'maxLinksByUser' => array(
            'type'=>'int',
            'label'=>"Maximum number of allowed generated links by user",
            'default'=>'1',
            'htmlOptions' => array(
                'min'=>1,
            ),
        ),
        //~ 'addPassword' => array(
        //~ ),
    );
    public function init()
    {
        if (Yii::app() instanceof CConsoleApplication) {
          return;
        }
        $this->subscribe('beforeActivate');
        $this->subscribe('createNewUser');
        $this->subscribe('beforeControllerAction');
        /* Register language string and set or fix DB */
        $this->subscribe('afterPluginLoad');

        $this->subscribe('newDirectRequest');
    }
    /**
     * @see https://manual.limesurvey.org/BeforeControllerAction
     * @return void
     * @throws CHttpException default invalid request from Yii
     */
    public function createNewUser()
    {
        $coreEvent = $this->getEvent();
        if ($coreEvent->get('errorCode') != AuthPluginBase::ERROR_NONE) {
            return;
        }
        $userId = $coreEvent->get('newUserID');
        if(empty($userId)) {
            throw new CHttpException(500,"There are an issue with linkPasswordUser plugin order.");
        }
        $this->_addUser($userId);
    }

    public function beforeControllerAction()
    {
        $controller = $this->getEvent()->get('controller');
        $action = $this->getEvent()->get('action');
        $sa = Yii::app()->getRequest()->getParam('sa');
        if($controller != 'admin' || $action !='authentication' || $sa !='forgotpassword') {
            return;
        }
        if(!Yii::app()->getRequest()->getIsPostRequest()) {
            return;
        }
        $this->_forgotPassword();
        Yii::app()->end();
    }
    /**
    * newDirectRequest for password form
    */
    public function newDirectRequest()
    {
        $oEvent = $this->event;
        if ($this->event->get('target') != get_class()) {
            return;
        }
        $oEvent = $this->event;
        $sFunction=$oEvent->get('function');
        switch ($sFunction) {
            case 'setpassword':
                $this->setPasswordAction();
                break;
            default:
                throw new CHttpException(500,gT("Invalid action"));
        }
    }

    public function setPasswordAction()
    {
        if (!empty(Yii::app()->session['loginID'])) {
            Yii::app()->setFlashMessage($this->_translate("You are already logged in"));
            Yii::app()->getController()->redirect(array('admin/index'));
        }
        $hash = Yii::app()->getRequest()->getParam('hash');

        /* Start by hash */
        if(!$hash) {
            Yii::app()->setFlashMessage($this->_translate("Invalid hash"));
            Yii::app()->getController()->redirect(array("admin/index"));
        }
        $oUser = \linkPasswordUser\models\onetimeHash::getUserByHash($hash);
        if(!$oUser) {
            Yii::app()->setFlashMessage($this->_translate("Invalid hash"));
            Yii::app()->getController()->redirect(array("admin/index"));
        }
        $aData = array();
        if(Yii::app()->getRequest()->getIsPostRequest()) {
            $passwordError = $this->_getPasswordValidationError();
            if(empty($passwordError)) {
                \linkPasswordUser\models\onetimeHash::model()->deleteAll("username = :username",array(":username"=>$oUser->users_name));
                $oUser->setPassword(Yii::app()->getRequest()->getPost('newPassword'),true);
                Yii::app()->setFlashMessage($this->_translate("Your password is set, use it to log in."));
                Yii::app()->getController()->redirect(array("admin/index"));
            }
            $aData['passwordError'] = $passwordError;
        }
        $aData['lang'] = array(
            'Set you password' => $this->_translate("Set you password"),
            'New password:' => $this->_translate("New password:"),
            'Repeat new password:' => $this->_translate("Repeat new password:"),
            'Change password' => $this->_translate("Change password"),
        );
        $aData['currentHash'] = $hash;
        $aData['formUrl'] = Yii::app()->getController()->createUrl('plugins/direct', array(
            'plugin' => $this->getName(),
            'function' => 'setpassword',
            'hash' => $hash
        ));
        $this->_displayContent($aData,array("setPasswordForm"));
        Yii::app()->end();
    }

    /**
     * Validatioon of form
     * @return null|string
     */
    private function _getPasswordValidationError()
    {
        $newPassword = Yii::app()->getRequest()->getPost('newPassword');
        $confirmPassword = Yii::app()->getRequest()->getPost('confirmPassword');
        if(empty($newPassword)) {
            return gT("The password can't be empty.");
        }
        if($newPassword !== $confirmPassword) {
            return gT("Your passwords do not match.");
        }
    }


    /**
     * Add this translation just after loaded all plugins
     * @see event afterPluginLoad
     */
    public function afterPluginLoad(){
        $this->_setConfig();
    }

    /** @inheritdoc **/
    public function beforeActivate()
    {
        $this->_setDb();
    }

    private function _forgotPassword()
    {
        if (!empty(Yii::app()->session['loginID'])) {
            Yii::app()->getController()->redirect(array('admin/index'));
        }
        $sUserName = Yii::app()->request->getPost('user');
        $sEmailAddr = Yii::app()->request->getPost('email');
        $oUser = User::model()->find("users_name = :users_name and email = :email",array(':users_name' => $sUserName, ':email' => $sEmailAddr));
        // Preventing attacker from easily knowing whether the user and email address are valid or not (and slowing down brute force attacks)
        usleep(rand(Yii::app()->getConfig("minforgottenpasswordemaildelay"),Yii::app()->getConfig("maxforgottenpasswordemaildelay")));
        $aData = array();
        if(empty($oUser)) {
            $aData['message'] = 'If username and email are valid and you are allowed to use internal database authentication a new link has been sent to you';
            $this->_displayContent($aData,array('resetPasswordMessage'));
        }
        $countCurrent = \linkPasswordUser\models\onetimeHash::model()->count("username = :username",array(":username"=>$oUser->users_name));
        if($countCurrent >= Yii::app()->getConfig('linkPasswordUser_maxLinksByUser')) {
            $aData['message'] = 'If username and email are valid and you are allowed to use internal database authentication a new link has been sent to you';
            $this->_displayContent($aData,array('resetPasswordMessage'));
        }
        if(!Permission::model()->hasGlobalPermission('auth_db','read',$oUser->uid)) {
            $aData['message'] = 'If username and email are valid and you are allowed to use internal database authentication a new link has been sent to you';
            $this->_displayContent($aData,array('resetPasswordMessage'));
        }
        $onetimeHash = new \linkPasswordUser\models\onetimeHash();
        $onetimeHash->username = $oUser->users_name;
        if(!$onetimeHash->save()) {
            throw new CHttpException(500,\CHtml::errorSummary($onetimeHash,'<p>'."Unable to save the one time hash.".'</p>');
        }
        $onetimeHash = \linkPasswordUser\models\onetimeHash::model()->find("username = :username",array(":username"=>$oUser->users_name));
        if(empty($onetimeHash)) {
            tracevar($onetimeHash);
            Yii::app()->end();
        }
        $url = Yii::app()->getController()->createAbsoluteUrl('plugins/direct', array(
            'plugin' => $this->getName(),
            'function' => 'setpassword',
            'hash' => $onetimeHash->uniquehash
        ));
    
        $subject = gT('User data');
        $body = sprintf(gT("Hello %s,"), CHtml::encode($oUser->full_name)) . "<br><br>\n";
        $body .= sprintf(gT("this is an automated email from '%s'."), Yii::app()->getConfig("sitename")) . "<br /><br />\n";
        $body .= gT("You can use now the following credentials to log into the site:") . "<br>\n";
        $body .= gT("Username") . ": " . CHtml::encode($oUser->users_name) . "<br>\n";
        $body .= $this->_translate("Use this link to set your password:") . "<br>\n";
        $body .= CHtml::link($url,$url);
        $body .= "<br>\n";
        $body .= $this->_translate("If you don't ask to reset your password, just delete this email.") . "<br>\n";

        $to = $oUser->full_name . " <".$oUser->email.">";
        $from = Yii::app()->getConfig("siteadminname") . " <" . Yii::app()->getConfig("siteadminemail") . ">";
        $message = '';
        $state = '';
        $header = '';
        if (SendEmailMessage($body, $subject, $to, $from, Yii::app()->getConfig("sitename"), true, Yii::app()->getConfig("siteadminbounce"))) {
            $aData['message'] = 'If username and email are valid and you are allowed to use internal database authentication a new link has been sent to you';
        } else {
            $aData['message'] = 'Unable to send email, please contact administrator.';
        }
        
        $this->_displayContent($aData,array('resetPasswordMessage'));

    }
    private function _addUser($userId)
    {
        $oUser = User::model()->findByPk($userId);
        \linkPasswordUser\models\onetimeHash::model()->deleteAll("username = :username",array(":username"=>$oUser->users_name));
        /* Create a link */
        $onetimeHash = new \linkPasswordUser\models\onetimeHash();
        $onetimeHash->username = $oUser->users_name;
        $onetimeHash->save();
        $onetimeHash = \linkPasswordUser\models\onetimeHash::model()->find("username = :username",array(":username"=>$oUser->users_name));

        Permission::model()->insertSomeRecords(array('uid' => $userId, 'permission' => Yii::app()->getConfig("defaulttemplate"), 'entity'=>'template', 'read_p' => 1, 'entity_id'=>0));

        $subject = sprintf(gT("User registration at '%s'", "unescaped"), Yii::app()->getConfig("sitename"));
        $body = sprintf(gT("Hello %s,"), CHtml::encode($oUser->full_name)) . "<br><br>\n";
        $body .= sprintf(gT("this is an automated email to notify that a user has been created for you on the site '%s'."), Yii::app()->getConfig("sitename")) . "<br /><br />\n";
        $body .= gT("You can use now the following credentials to log into the site:") . "<br>\n";
        $body .= gT("Username") . ": " . CHtml::encode($oUser->users_name) . "<br>\n";
        $body .= $this->_translate("Use this link to set your password:") . "<br>\n";

        $url = Yii::app()->getController()->createAbsoluteUrl('plugins/direct', array(
            'plugin' => $this->getName(),
            'function' => 'setpassword',
            'hash' => $onetimeHash->uniquehash
        ));
        $body .= CHtml::link($url,$url);

        $to = $oUser->full_name . " <".$oUser->email.">";
        $from = Yii::app()->getConfig("siteadminname") . " <" . Yii::app()->getConfig("siteadminemail") . ">";
        $message = '';
        $state = '';
        $header = '';
        if (SendEmailMessage($body, $subject, $to, $from, Yii::app()->getConfig("sitename"), true, Yii::app()->getConfig("siteadminbounce"))) {
            $message .= gT("Username") . ": {$oUser->users_name}<br />" . gT("Email") . ": {$oUser->email}<br>";
            $message .= "<br>" . $this->_translate("An email with a generated link was sent to the user.");
            $state = 'success';
            $header= gT("Success");
        }
        else
        {
            // has to be sent again or no other way
            $tmp = str_replace("{NAME}", "<strong>" . $oUser->users_name . "</strong>", gT("Email to {NAME} ({EMAIL}) failed."));
            $message .= str_replace("{EMAIL}", $oUser->email, $tmp) ;
            $state = 'warning';
            $header= gT("Warning");
        }
        $aData = array(
            'message' => $message,
            'state' => $state,
            'header' => $header,
            'oUser' => $oUser,
        );
        $this->_displayContent($aData,array("boxWithRedirect"));
        Yii::app()->end();
    }
    /**
    * Add needed alias and put it in autoloader,
    * add surveysessiontime_limit to global config
    * @return void
    */
    private function _setConfig()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $this->_setDb();
        $messageSource=array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this).'Lang',
            'cachingDuration'=>0,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
            'catalog'=>'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this).'Messages',$messageSource);
        Yii::app()->setConfig('linkPasswordUser_linkTime',
            $this->get('linkTime',null,null,$this->settings['linkTime']['default'])
        );
        Yii::app()->setConfig('linkPasswordUser_randomKeySize',
            $this->get('randomKeySize',null,null,$this->settings['randomKeySize']['default'])
        );
        Yii::app()->setConfig('linkPasswordUser_maxLinksByUser',
            $this->get('maxLinksByUser',null,null,$this->settings['maxLinksByUser']['default'])
        );
    }

    /**
    * Create or update needed DB
    * @return void
    */
    private function _setDb()
    {
        if($this->get("dbVersion") == self::$dbVersion) {
            return;
        }
        /* dbVersion not needed */
        if(!$this->api->tableExists($this, 'onetimeHash')) {
            $this->api->createTable($this, 'onetimeHash', array(
                'id'=>'pk',
                'username'=>"string(64) NOT NULL default ''",
                'uniquehash'=>"text NOT NULL",
                'password'=>"text NOT NULL",
                'dateset' => "datetime",
            ));
            $this->set("dbVersion",self::$dbVersion);
            return;
        }

        /* all done */
        $this->set("dbVersion",self::$dbVersion);
    }

    private function _displayContent($aData=false,$views=array())
    {
        // TODO : improve and move to PluginsController
        if(!$aData){$aData=array();}
        $oAdminController=new AdminController('admin');
        $aData['sImageURL'] = Yii::app()->getConfig('adminimageurl');
        /* Mimic 2.06 system : must fix */
        ob_start();
        header("Content-type: text/html; charset=UTF-8"); // needed for correct UTF-8 encoding
        $oAdminController->_getAdminHeader();
        if (!empty(Yii::app()->session['loginID'])) {
            $oAdminController->_showadminmenu();
        }
        foreach($views as $view){
            Yii::setPathOfAlias("views.{$view}", dirname(__FILE__).DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR.$view);
            $oAdminController->renderPartial("views.{$view}",$aData);
        }
        $oAdminController->_loadEndScripts();
        $oAdminController->_getAdminFooter('http://manual.limesurvey.org', gT('LimeSurvey online manual'));
        $sOutput = ob_get_contents();
        ob_clean();
        App()->getClientScript()->render($sOutput);
        echo $sOutput;
        Yii::app()->end();
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function _translate($string){
        return Yii::t('',$string,array(),get_class($this));
    }

}
