<?php
/**
 * This file is part of reloadAnyResponse plugin
 * @version 1.3.0
 */
namespace linkPasswordUser\models;
use Yii;
use CActiveRecord;
use User;

class onetimeHash extends CActiveRecord
{
    /**
     * Class reloadAnyResponse\models\onetimeLink
     *
     * @property integer $id single id
     * @property string $username : the user name
     * @property string $uniquehash unique link
     * @property datetime $dateset : when is set
     * @property string $password : extra password
    */

    /** @inheritdoc */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * init to set default
     *
     */
    public function init()
    {
        $this->username = "";
        $this->uniquehash = "";
        $this->dateset = null;
        $this->password = "";
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{linkpassworduser_onetimeHash}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return array('id');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('username', 'required'),
            array('uniquehash', 'unique'),
        );
        return $aRules;
    }

    public function beforeSave() {
        if($this->id) {
            $countUser = self::model()->count("username = :username id <> :id",array(":username"=>$this->username,":id"=>$this->id));
        } else {
            $countUser = self::model()->count("username = :username",array(":username"=>$this->username));
        }
        if($countUser > 0 && $countUser >= Yii::app()->getConfig('linkPasswordUser_maxLinksByUser')) {
            $this->addError("username","Max number of this username already set");
            die("Max number of this username already set");
            return false;
        }
        if(empty($this->uniquehash)){
            $this->setHash();
        }
        if(empty($this->uniquehash)){
            die("toto".Yii::app()->getConfig('linkPasswordUser_randomKeySize'));
        }
        $this->dateset = date('Y-m-d H:i:s');
        return parent::beforeSave();
    }

    /**
     * Set the hash using default settings
     * @return void
     */
    public function setHash()
    {
        $hash = Yii::app()->securityManager->generateRandomString(Yii::app()->getConfig('linkPasswordUser_randomKeySize'));
        $counter = 0;
        while($this->getUserByHash($hash)) {
            $hash = Yii::app()->securityManager->generateRandomString(Yii::app()->getConfig('linkPasswordUser_randomKeySize'));
            $counter++;
            // This is extremely unlikely.
            if ($counter > 10) {
                throw new CHttpException(500, 'Failed to create unique hash in 10 attempts.');
            }
        }
        $this->uniquehash = $hash;
    }

    /**
     * get the user by hash
     * @param $hash
     * @return \User|false
     */
    public static function getUserByHash($hash)
    {
        $delay = Yii::app()->getConfig('linkPasswordUser_linkTime');
        $delay++;
        $maxDateTime=date('Y-m-d H:i:s', strtotime("{$delay} minutes ago"));
        self::model()->deleteAll("dateset < :dateset",
            array(":dateset"=>$maxDateTime)
        );
        $oOnetimeHash = self::model()->find("uniquehash = :uniquehash",array(":uniquehash"=>$hash));
        if(empty($oOnetimeHash)) {
            return false;
        }
        $oUser = User::model()->find("users_name = :username",array(":username"=>$oOnetimeHash->username));
        if(empty($oUser)) {
            return false;
        }
        return $oUser;
    }


}
