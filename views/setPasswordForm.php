<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <div class="messagebox ui-corner-all">
                <div class="header ui-widget-header"><?php echo $lang['Set you password']; ?></div>
                <?php if(!empty($passwordError)) { ?>
                    <div class="alert alert-danger">
                        <?php echo $passwordError; ?>
                    </div>
                <?php } ?>
                    <?php echo CHtml::beginForm($formUrl,'post',array('id'=>'loginform')); ?>
                    <ul class="unstyled">
                        <li>
                            <label for="newPassword"><?php echo $lang['New password:']; ?></label>
                            <input id="newPassword" type="password" name="newPassword">
                        </li>
                        <li>
                            <label for="confirmPassword"><?php echo $lang['Repeat new password:']; ?></label>
                            <input id="confirmPassword" type="password" name="confirmPassword">
                        </li>
                    </ul>
                    <p>
                        <?php echo CHtml::htmlButton($lang['Change password'],array("type"=>"submit")); ?>
                    </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
