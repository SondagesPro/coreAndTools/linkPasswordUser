<div class="header ui-widget-header"><?php echo gT("Add user") ?></div>
<div class="container">
    <div class="row">
        <div class="alert alert-<?php echo $state ?> span8 offset2">
            <div class="h4 lead"><strong><?php echo $header ?></strong></div>
            <p><?php echo $message ?></p>
            <?php
                echo CHtml::beginForm(array("admin/user/sa/setuserpermissions"),'post',array("style"=>"text-align:center"));
                echo CHtml::submitButton(gT("Set user permissions"),array("class"=>"ui-button ui-widget ui-state-default ui-corner-all limebutton btn-block"));
                echo CHtml::hiddenField("user",$oUser->users_name);
                echo CHtml::hiddenField("uid",$oUser->uid);
                echo CHtml::endForm();
            ?>
        </div>
    </div>
</div>

