# linkPasswordUser

Replace LimeSurvey core system : Send a one time password link **Only for LimeSurvey 2.6lts**

- Replace email send when user was created by an email with one time link
- Replace lost password email by an email with one time link
- Don't update password when reset password with lost password link
- Allow to limit time of one time link
- Allow to restrict one time link email number
